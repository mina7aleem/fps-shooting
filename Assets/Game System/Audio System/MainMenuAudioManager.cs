using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace GameSystem
{
    namespace AudioSystem
    {
        public class MainMenuAudioManager : MonoBehaviour
        {
            private AudioSource audioSource;
            [SerializeField] TextMeshProUGUI text;
            void Start()
            {
                audioSource = GetComponent<AudioSource>();
            }

            /* public void PlayButtonSound()
            {
                audioSource.Play();
            } */

            void Awake()
            {
                DontDestroyOnLoad(this.gameObject);
            }

            public void MusicOnOff()
            {
                if (audioSource.enabled == true)
                {
                    text.text = "Play Music";
                    audioSource.enabled = false;
                }
                else 
                {
                    text.text = "Mute Music";
                    audioSource.enabled = true;
                }
            }
        }
    }
}


