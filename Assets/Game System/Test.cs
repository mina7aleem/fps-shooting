using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    [SerializeField] Transform target;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance > 2)
        {
            Vector3 direction = Vector3.MoveTowards(transform.position, target.position, 1);
            transform.Translate(-direction *Time.deltaTime);
        }
    }
}
