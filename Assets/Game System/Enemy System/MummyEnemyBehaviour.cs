using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace GameSystem
{
    namespace EnemySystem
    {
        public class MummyEnemyBehaviour : MonoBehaviour
        {
            float chaseRange = 6;
            float attackRange = 2;
            Animator animator;
            GameObject player;
            NavMeshAgent navMeshAgent;
            float enemyPlayerDistance;
            [SerializeField] Image healthBarImage;
            bool isAlive = true;
            [SerializeField] GameObject healthBarCanvas;
            Collider _collider;
            void Start()
            {
                animator = GetComponent<Animator>();
                player = GameObject.Find("Player");
                navMeshAgent = GetComponent<NavMeshAgent>();
                _collider = GetComponent<Collider>();
            }

            void Update()
            {
                if (isAlive)
                {
                    ChasePlayer();
                    Attack();
                }
            }

            
            void ChasePlayer()
            {
                enemyPlayerDistance = Vector3.Distance(transform.position, player.transform.position);
                if (enemyPlayerDistance < chaseRange)
                {
                    navMeshAgent.destination = player.transform.position;
                    chaseRange = 50;
                }
            }
            
            void Attack()
            {
                enemyPlayerDistance = Vector3.Distance(transform.position, player.transform.position);
                
                if (enemyPlayerDistance < attackRange)
                {
                    animator.SetBool("Attack", true);
                    transform.LookAt(player.transform);
                }
                else
                {
                    animator.SetBool("Attack", false);
                }
            }

            void OnTriggerEnter(Collider other) 
            {
                if (other.transform.tag == "PlayerBullet" || other.transform.tag == "PlayerSword")
                {
                    if (healthBarImage.fillAmount > 0)
                    {
                        healthBarImage.fillAmount -=0.2f;
                    }
                    if (healthBarImage.fillAmount < Mathf.Epsilon)
                    {
                        _collider.enabled = false;
                        isAlive = false;
                        animator.SetTrigger("Die");
                        healthBarCanvas.SetActive(false);
                        ScoringSystem.ScoreManager.score++;
                        StartCoroutine(WaitForDeactivate());
                    }
                }
            }

            IEnumerator WaitForDeactivate()
            {
                yield return new WaitForSeconds(5);
                gameObject.SetActive(false);
            }
        }
    }
}

