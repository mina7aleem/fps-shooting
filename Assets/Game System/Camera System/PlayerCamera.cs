using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace CameraSystem
    {
        public class PlayerCamera : MonoBehaviour
        {
            public float sensX;
            public float sensY;
            public Transform orientation;
            float xRotation;
            float yRotation;
            void Start()
            {
                LockAndHideCursor();  
            }

            void Update()
            {
                MouseMovement();
            }

            void LockAndHideCursor()
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

            void MouseMovement()
            {
                float mouseX = Input.GetAxisRaw("Mouse X") * Time.deltaTime * sensX;
                float mouseY = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * sensY;
                yRotation += mouseX;
                xRotation -= mouseY;
                xRotation = Mathf.Clamp(xRotation, -70f, 70f);
                transform.rotation = Quaternion.Euler(xRotation, yRotation, 0);
                orientation.rotation = Quaternion.Euler(0, yRotation, 0);
            }
        }
    }
}


