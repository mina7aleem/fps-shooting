using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace UISystem
    {
        public class UIPauseMenuManager : MonoBehaviour
        {
            public int currentSceneIndex;
            public bool gameIsPaused = false;
            public GameObject pauseMenuButtonsPanel;
            public GameObject pauseButton;
            public GameObject gamePlayAudioManager;

            void Awake()
            {
                pauseMenuButtonsPanel.SetActive(false);
                currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            }

            public void PauseGame()
            {
                if (!gameIsPaused)
                {
                    gamePlayAudioManager.GetComponent<AudioSource>().enabled = false;
                    pauseMenuButtonsPanel.SetActive(true);
                    Time.timeScale = 0f;
                    gameIsPaused = true;
                    pauseButton.SetActive(false);
                }
                else if (gameIsPaused)
                {
                    gamePlayAudioManager.GetComponent<AudioSource>().enabled = true;
                    pauseMenuButtonsPanel.SetActive(false);
                    Time.timeScale = 1f;
                    gameIsPaused = false;
                    pauseButton.SetActive(true);
                }                
            }

            public void BackToMainMenu()
            {
                Time.timeScale = 1f;
                SceneManager.LoadScene(0);
            }

            public void RestartLevel()
            {
                SceneManager.LoadScene(currentSceneIndex);
                Time.timeScale = 1f;
                //ScoringSystem.ScoreManager.score = 0;
            }   

            public void QuitGame()
            {
                Application.Quit();
                Debug.Log("QuitGame");
            }    
        }
    }
}


