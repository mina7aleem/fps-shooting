using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace UISystem
    {
        public class UIManager : MonoBehaviour
        {
            private AudioSource audioSource;
            [SerializeField] GameObject settingsPanel;
            void Start()
            {
                audioSource = GetComponent<AudioSource>();
            }

            public void PlayButtonSound()
            {
                audioSource.Play();
            }

            public void ChooseLevel()
            {
                SceneManager.LoadScene("Choose Level");
                //ScoringSystem.ScoreManager.score = 0;
            }
                    
            public void QuitGame()
            {
                Application.Quit();
                Debug.Log("QuitGame");
            }

            public void StartLevel1()
            {
                SceneManager.LoadScene("Level 1");
            }

            public void StartLevel2()
            {
                SceneManager.LoadScene("Level 2");
            }

            public void OpenSettingsPanel()
            {
                if (!settingsPanel.activeInHierarchy)
                {
                    settingsPanel.SetActive(true);
                }
                else
                {
                    settingsPanel.SetActive(false);
                }
            }
        }
    }
}


