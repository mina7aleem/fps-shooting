using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace UISystem
    {
        public class UICanvasLevel2 : MonoBehaviour
        {
            [SerializeField] GameObject enemy1;
            [SerializeField] GameObject enemy2;
            [SerializeField] GameObject enemy3;
            [SerializeField] GameObject enemy4;
            [SerializeField] GameObject enemy5;
            [SerializeField] GameObject enemy6;
            [SerializeField] GameObject enemy7;
            [SerializeField] GameObject enemy8;
            [SerializeField] GameObject enemy9;
            [SerializeField] GameObject enemy10;
            [SerializeField] GameObject congratsPanel;
            void Start()
            {
                LockAndHideCursor();
            }

            void Update() 
            {
                CheckIfPlayerWin();
            }

            void LockAndHideCursor()
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

            public void PlayLevel1()
            {
                SceneManager.LoadScene("Level 1");
                ScoringSystem.ScoreManager.score = 0;
            }

            public void PlayLevel2()
            {
                SceneManager.LoadScene("Level 2");
                ScoringSystem.ScoreManager.score = 0;

            }

            public void QuitGame()
            {
                Application.Quit();
                Debug.Log("QuitGame");
            }

            void CheckIfPlayerWin()
            {
                if (!enemy1.activeInHierarchy && !enemy2.activeInHierarchy && !enemy3.activeInHierarchy && !enemy4.activeInHierarchy
                    && !enemy5.activeInHierarchy&& !enemy6.activeInHierarchy&& !enemy7.activeInHierarchy&& !enemy8.activeInHierarchy
                    && !enemy9.activeInHierarchy&& !enemy10.activeInHierarchy)
                {
                    congratsPanel.SetActive(true);
                    UnlockCursor();
                }
            }

            void UnlockCursor()
            {
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }
        }
    }
}


