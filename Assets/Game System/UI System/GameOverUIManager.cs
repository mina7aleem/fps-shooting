using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace UISystem
    {
        public class GameOverUIManager : MonoBehaviour
        {
            

            public void QuitGame()
            {
                Application.Quit();
                Debug.Log("QuitGame");
            }

            public void StartLevel1()
            {
                SceneManager.LoadScene("Level 1");
            }
        }
    }
}

