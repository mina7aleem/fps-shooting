using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace UISystem
    {
        public class UICanvasLevel1 : MonoBehaviour
        {
            [SerializeField] GameObject enemy1;
            [SerializeField] GameObject enemy2;
            [SerializeField] GameObject enemy3;
            [SerializeField] GameObject enemy4;
            [SerializeField] GameObject congratsPanel;

            void Start() 
            {
                LockAndHideCursor();
            }

            void Update()
            {
                CheckIfPlayerWin();
            }

            void CheckIfPlayerWin()
            {
                if (!enemy1.activeInHierarchy && !enemy2.activeInHierarchy && !enemy3.activeInHierarchy && !enemy4.activeInHierarchy)
                {
                    congratsPanel.SetActive(true);
                    UnlockCursor();
                }
            }

            void LockAndHideCursor()
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

            void UnlockCursor()
            {
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }

            public void GoToLevel2()
            {
                SceneManager.LoadScene("Level 2");
            }
        }
    }
}


