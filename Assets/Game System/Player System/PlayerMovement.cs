using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace PlayerSystem
    {
        public class PlayerMovement : MonoBehaviour
        {
            float moveSpeed = 1000;
            float maxSpeed = 5;
            float jumpForce = 1000;
            public Transform orientation;
            float horizontalInput;
            float verticalInput;
            Vector3 moveDirection;
            Rigidbody rB;
            void Start()
            {
                rB = GetComponent<Rigidbody>();
                rB.freezeRotation = true;
            }

            void Update()
            {
                MoveInput();
                horizontalInput = Input.GetAxisRaw("Horizontal") * Time.deltaTime * moveSpeed;
                verticalInput = Input.GetAxisRaw("Vertical") * Time.deltaTime * moveSpeed;
            }

            void FixedUpdate() 
            {
                PlayerMove();
                PlayerJump();
            }

            void MoveInput()
            {
                horizontalInput = Input.GetAxisRaw("Horizontal");
                verticalInput = Input.GetAxisRaw("Vertical");
            }
            
            void PlayerMove()
            {
                moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;
                rB.AddForce(moveDirection.normalized * moveSpeed, ForceMode.Force);
                rB.velocity = Vector3.ClampMagnitude(rB.velocity, maxSpeed);
            }

            void PlayerJump()
            {
                if (Input.GetKeyDown (KeyCode.Space) && rB.velocity.y == 0)
                {
                    rB.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                }
            }
        }
    }
}

