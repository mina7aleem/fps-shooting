using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace PlayerSystem
    {
        public class PlayerFireManager : MonoBehaviour
        {
            private List<GameObject> playerBulletPool = new List<GameObject>();
            [SerializeField] GameObject playerBullet;
            private int playerBulletPoolSize = 20;
            [SerializeField] Transform firePoint;
            private int bulletSpeed = 1000;
            [SerializeField] AudioClip SniperFire;
            [SerializeField] AudioClip SmgFire;
            [SerializeField] AudioClip SwordWaving;
            private AudioSource audioSource;
            [SerializeField] PlayerWeaponManager playerWeaponManager;
            float sniperFireRate = 1f;
            float sniperNextFire = 0;
            float smgFireRate = 0.3f;
            float smgNextFire = 0;
            [SerializeField] GameObject hitMarker;
            void Start()
            {
                FillingPlayerBulletPool();
                audioSource = GetComponent<AudioSource>();
            }

            void Update()
            {
                PlayerAttack();
            }

            void FillingPlayerBulletPool()
            {
                for (int i = 0; i < playerBulletPoolSize; i++)
                {
                    GameObject spawnedPlayerBullet = Instantiate(playerBullet);
                    spawnedPlayerBullet.SetActive(false);
                    playerBulletPool.Add(spawnedPlayerBullet);
                }
            }

            public GameObject GetPooledPlayerBullet()
            {
                for (int i = 0; i < playerBulletPool.Count; i++)
                {
                    if (!playerBulletPool[i].activeInHierarchy)
                    {
                        return playerBulletPool[i];
                    }
                }
                return null;
            }

            void PlayerAttack()
            {
                if (Input.GetMouseButtonDown(0))
                    {
                        if (playerWeaponManager.GetActiveWeaponIndex() == 1)
                        {
                            if (Time.time > sniperFireRate + sniperNextFire)
                            {
                                GameObject playerBullet = GetPooledPlayerBullet();
                                playerBullet.transform.position = firePoint.position;
                                playerBullet.GetComponent<Rigidbody>().velocity = firePoint.transform.forward * Time.deltaTime * bulletSpeed;
                                playerBullet.SetActive(true);
                                audioSource.PlayOneShot(SniperFire);
                                sniperNextFire = Time.time;
                                ShowHitMarker();
                            }
                        }
                        else if (playerWeaponManager.GetActiveWeaponIndex() == 2)
                        {
                            if (Time.time > smgFireRate + smgNextFire)
                            {
                                GameObject playerBullet = GetPooledPlayerBullet();
                                playerBullet.transform.position = firePoint.position;
                                playerBullet.GetComponent<Rigidbody>().velocity = firePoint.transform.forward * Time.deltaTime * bulletSpeed;
                                playerBullet.SetActive(true);
                                audioSource.PlayOneShot(SmgFire);
                                ShowHitMarker();
                            }
                        }
                        else if (playerWeaponManager.GetActiveWeaponIndex() == 0)
                        {
                            hitMarker.SetActive(false);
                            playerWeaponManager.Weapons[0].GetComponent<Animator>().SetTrigger("HitSword");
                            audioSource.PlayOneShot(SwordWaving);
                        }
                    }
                /* if (gameObject.GetComponent<Collider>().enabled == true)
                {
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        GameObject playerBullet = GetPooledPlayerBullet();
                        playerBullet.transform.position = firePoint.position;
                        playerBullet.SetActive(true);
                        audioSource.PlayOneShot(fire);
                    }
                } */
            }

            void ShowHitMarker()
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 10))
                {
                    hitMarker.SetActive(true);
                }
            }
        }
    }
}

