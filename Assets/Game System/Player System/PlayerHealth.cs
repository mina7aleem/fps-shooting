using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace PlayerSystem
    {
        public class PlayerHealth : MonoBehaviour
        {
            [SerializeField] Image healthBarImage;

            void OnCollisionEnter(Collision other) 
            {
                if (other.gameObject.tag == ("Enemy"))
                {
                    if (healthBarImage.fillAmount > 0)
                    {
                        healthBarImage.fillAmount -=0.2f;
                    }
                    if (healthBarImage.fillAmount < Mathf.Epsilon)
                    {
                        UnlockCursor();
                        SceneManager.LoadScene("GameOver");
                    }
                }
            }

            void UnlockCursor()
            {
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }
        }
    }
}


