using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace PlayerSystem
    {
        public class PlayerBulletBehaviour : MonoBehaviour
        {
            private int moveSpeed = 1; 
            void Start()
            {
                
            }

            void OnEnable() 
            {
                StartCoroutine("WaitForDeactivate");
            }

            void Update()
            {
                //MoveDirection();
            }

            void MoveDirection()
            {
                transform.Translate(Vector3.forward * Time.deltaTime);
            }

            void OnTriggerEnter(Collider other) 
            {
                gameObject.SetActive(false);
                /* if (other.tag == "Enemy")
                {
                    gameObject.SetActive(false);
                } */
            }

            IEnumerator WaitForDeactivate()
            {
                yield return new WaitForSeconds(3);
                gameObject.SetActive(false);
            }
        }

    }
}

