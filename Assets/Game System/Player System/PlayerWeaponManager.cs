using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace PlayerSystem
    {
        public class PlayerWeaponManager : MonoBehaviour
        {
            public List<GameObject> Weapons;
            public int currentindex;
            void Start()
            {
                
            }

            void Update()
            {
                ScrollWeapons();
            }

            private void ScrollWeapons()
            {

                if (Input.mouseScrollDelta.y > 0)
                {
                   if (Weapons.Count - 1 > currentindex)
                       currentindex++;
                    for (int i = 0; i < Weapons.Count; i++)
                    {
                        Weapons[i].SetActive(false);
                    }
                   Weapons[currentindex].SetActive(true);

                }
                if (Input.mouseScrollDelta.y < 0)
                {
                   if (0 < currentindex)
                        currentindex--;
                    for (int i = 0; i < Weapons.Count; i++)
                    {
                        Weapons[i].SetActive(false);
                    }
                    Weapons[currentindex].SetActive(true);
                }
            }

            public int GetActiveWeaponIndex()
            {
                return currentindex;
            }
        }
    }
}


