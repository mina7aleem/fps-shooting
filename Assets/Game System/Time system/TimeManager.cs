using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace GameSystem
{
    namespace TimeSystem
    {
        public class TimeManager : MonoBehaviour
        {
            public float time;
            public TextMeshProUGUI timeText;
            void Start()
            {
                
            }

            void Update()
            {
                SetTime();
            }

            void SetTime()
            {
                time+= Time.deltaTime;
                int intTime = Mathf.RoundToInt(time);
                timeText.text = "Time: " + intTime;
            }
        }
    }
}


